import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:acn_training_june_2023/model/university.dart';
import 'dart:convert';

class UserStorage {
  final _storage = const FlutterSecureStorage();
  final String _usernameKey = "USERNAME";
  
  List<University> savedUniversities = [];

  Future<void> setUserName(String username) async {
    await _storage.write(key: _usernameKey, value: username);
  }

  Future<String?> getUserName() async {
    String? storedUsername = await _storage.read(key: _usernameKey);
    return storedUsername;
  }

  Future<void> removeUserName() async {
    await _storage.delete(key: _usernameKey);
  }

  Future<void> saveUniversities(String phonenumber, List<University> universities) async {
    // Convert the list to a JSON string

    String universitiesJson = json.encode(universities);

    // Save the JSON string in secure storage
    // phone number

    await _storage.write(key: phonenumber, value: universitiesJson);
  }

  Future<List<University>> getUniversities(String phonenumber) async {
    // Retrieve the JSON string from secure storage
    String? universitiesJson = await _storage.read(key: phonenumber);

    if (universitiesJson != null) {
      // Convert the JSON string back to a list of University objects
      List<dynamic> universitiesData = json.decode(universitiesJson);
      List<University> universities = universitiesData
          .map((data) => University.fromJson(data))
          .toList();

      return universities;
    }

    return [];
  }
}
