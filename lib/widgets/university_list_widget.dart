import 'package:acn_training_june_2023/model/university.dart';
import 'package:flutter/material.dart';

class UniversityListWidget extends StatelessWidget {
  final University university;

  const UniversityListWidget({
    super.key,
    required this.university,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.fromLTRB(25, 20, 25, 20),
        margin: const EdgeInsets.all(10),
        width: double.infinity,
        height: 190,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 0.5,
              blurRadius: 3,
              offset: const Offset(0, 2),
            )
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              university.name,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 8),
            Text(
              'Alpha Code: ${university.alphaCode}',
              style: TextStyle(fontSize: 14, color: Colors.grey),
            ),
            const SizedBox(height: 4),
            Text(
              'Domains: ${university.domains.join(", ")}',
              style: TextStyle(
                fontSize: 14,
                color: Colors.grey,
              ),
            ),
            const SizedBox(height: 4),
          Text(
            'State/Province: ${university.stateProvince}',
            style: TextStyle(
              fontSize: 14,
              color: Colors.grey,
            ),
          ),
            const SizedBox(height: 4),
          Text(
            'Web Pages: ${university.webPages.join(", ")}',
            style: TextStyle(
              fontSize: 14,
              color: Colors.grey,
            ),
          ),
            const SizedBox(height: 4),
          Text(
            'Country: ${university.country}',
            style: TextStyle(
              fontSize: 14,
              color: Colors.grey,
            ),
          ),
          ],
        ));
  }
}
