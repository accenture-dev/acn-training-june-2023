import 'package:flutter/material.dart';
import 'package:acn_training_june_2023/model/university.dart';
import 'package:acn_training_june_2023/utils/app_theme.dart';
import 'package:acn_training_june_2023/storage/user_storage.dart';
import 'package:acn_training_june_2023/screens/university/university_screen.dart';

class FavoriteWidget extends StatelessWidget {
  final University university;
  final Function removeUniversity;
  final String username;

  const FavoriteWidget({
    Key? key,
    required this.university,
    required this.removeUniversity,
    required this.username
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => UniversityScreen(
              university: university,
            ),
          ),
        );
      },
      child: ListTile(
        title: Text(university.name),
        subtitle: Text(university.country),
        trailing: IconButton(
          icon: Icon(
            Icons.star,
            color: AppTheme.starredColor,
          ),
          onPressed: () {
            removeUniversity(university, username);
          },
        ),
      ),
    );
  }
}
