import 'package:flutter/material.dart';

class AlertPopup extends StatelessWidget {
  const AlertPopup({Key? key}) : super(key: key);

  createAlertDialog(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(content),
            actions: <Widget>[
              MaterialButton(
                elevation: 5.0,
                onPressed: () => Navigator.pop(context, 'Cancel'),
                child: const Text('Cancel'),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
