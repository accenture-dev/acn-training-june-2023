import 'package:acn_training_june_2023/model/university.dart';
import 'package:flutter/material.dart';
import 'package:acn_training_june_2023/utils/app_theme.dart';
import 'package:acn_training_june_2023/screens/university/university_screen.dart';
import 'package:acn_training_june_2023/widgets/favorite_widget.dart';

import 'package:acn_training_june_2023/storage/user_storage.dart';
import 'package:acn_training_june_2023/utils/configuration.dart';



class FavoriteScreen extends StatefulWidget {
  final List<University> favorites;

  const FavoriteScreen({Key? key, required this.favorites}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  Config config = Config();
  String username = "";
  List<University> savedUniversities = [];
  List<dynamic>? universities;
  UserStorage userStorage = UserStorage();

  @override
  void initState() {
    super.initState();
    fetchUniversities().then((data) {
      setState(() {
        universities = data;
      });
    });

   // Retrieve the saved universities from the UserStorage
    userStorage.getUserName().then((un) {
      setState(() {
        username = un ?? "error";
      });

      userStorage.getUniversities(un.toString()).then((universities) {
      setState(() {
        savedUniversities = universities;
      });
    });

    });

  }

  @override
Widget build(BuildContext context) {
  return Scaffold(
    body: universities != null
        ? Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: savedUniversities.length,
                  itemBuilder: (context, index) {
                    final University university = savedUniversities[index];

                    return FavoriteWidget(
                      university: university,
                      removeUniversity: (university, username) {
                        setState(() {
                          savedUniversities.remove(university);
                          userStorage.saveUniversities(username,savedUniversities);
                        });
                      },
                      username: username,
                    );
                  },
                ),
              ),
            ],
          )
        : const CircularProgressIndicator(),
  );
}
}
