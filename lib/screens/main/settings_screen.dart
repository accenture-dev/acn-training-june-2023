import 'package:acn_training_june_2023/utils/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:acn_training_june_2023/storage/user_storage.dart';
import 'package:acn_training_june_2023/utils/navigation/navigation_service.dart';
import 'package:acn_training_june_2023/utils/navigation/app_path.dart' as Path;


class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen>  createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
UserStorage userStorage = UserStorage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Card(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Column(
              children: <Widget>[
                ListTile(
                    leading: const Icon(Icons.person_rounded, color: AppTheme.primaryDark),
                    title: const Text("Account Information"),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {}),
                ListTile(
                    leading: const Icon(Icons.lock_outline, color: AppTheme.primaryDark),
                    title: const Text("Change password"),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {}),
                ListTile(
                    leading: const Icon(Icons.enhanced_encryption, color: AppTheme.primaryDark),
                    title: const Text("Privacy"),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {}),
                ListTile(
                    leading: const Icon(Icons.notifications, color: AppTheme.primaryDark),
                    title: const Text("Notification"),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {}),
                ListTile(
                    leading: const Icon(Icons.help, color: AppTheme.primaryDark),
                    title: const Text("Help"),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    onTap: () {}),
                ListTile(
                    leading: const Icon(Icons.lock, color: AppTheme.secondary),
                    title: const Text("Logout"),
                    trailing: const Icon(Icons.input),
                    onTap: () {
                    userStorage.removeUserName().then((value) =>
                {
                  NavigationService.navigateByPageId(Path.LOGIN, context)});
                  }),
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
