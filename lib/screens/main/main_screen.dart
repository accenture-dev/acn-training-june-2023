import 'dart:async';
import 'dart:developer';

import 'package:acn_training_june_2023/screens/main/favorite_screen.dart';
import 'package:acn_training_june_2023/screens/main/home_screen.dart';
import 'package:acn_training_june_2023/screens/main/settings_screen.dart';
import 'package:acn_training_june_2023/storage/user_storage.dart';
import 'package:acn_training_june_2023/utils/app_theme.dart';
import 'package:acn_training_june_2023/utils/navigation/navigation_service.dart';
import 'package:flutter/material.dart';
import 'package:acn_training_june_2023/utils/navigation/app_path.dart' as Path;

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;
  PageController pageController = PageController();
  final List<Widget> _screens = [
    const HomeScreen(),
    FavoriteScreen(favorites: []),
    const SettingsScreen()
];


  UserStorage userStorage = UserStorage();
  Widget current = const HomeScreen();
  UserStorage userStore = UserStorage();
  Timer? timer;
  Map<String, dynamic> defaultColor = {
    "bgColor": 0xff9D58F5,
    "textColor": 0xffffffff,
    "text": "Hello"
  };

  void _changeColorByTime() {
    DateTime now = DateTime.now();
    if (now.hour.toDouble() >= 4 && now.hour.toDouble() < 11) {
      Color color = AppTheme.morningColor;
      setState(() {
        defaultColor['bgColor'] = color.value;
        defaultColor['text'] = "Good morning";
      });
    } else if (now.hour.toDouble() >= 11 && now.hour.toDouble() < 16) {
      setState(() {
        Color color = AppTheme.dayColor;
        Color colorBg = const Color(0xff27221D);
        defaultColor['bgColor'] = color.value;
        defaultColor['textColor'] = colorBg.value;
        defaultColor['text'] = "Good day";
      });
    } else if (now.hour.toDouble() >= 16 && now.hour.toDouble() < 19) {
      setState(() {
        Color color = AppTheme.eveningColor;
        defaultColor['bgColor'] = color.value;
        defaultColor['text'] = "Good evening";
      });
    } else if (now.hour.toDouble() >= 19 || now.hour.toDouble() < 4) {
      setState(() {
        Color color = AppTheme.nightColor;
        defaultColor['bgColor'] = color.value;
        defaultColor['text'] = "Hello";
      });
    }
  }

  void _onPageChanged(int index) {
    log("Current index $_selectedIndex");
  }

  void onTapped(int index) {
    log("Current tab $index");
    setState(() {
      _selectedIndex = index;
    });
    setState(() {
      current = _screens[index];
    });
    pageController.jumpToPage(index);
  }

  String username = "...";

 // set state phone number
  Future<void> getUsername() async {
    userStorage.getUserName().then((un) {
      setState(() {
        username = un ?? "error";
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getUsername();
    _changeColorByTime();
    timer = Timer.periodic(
        const Duration(minutes: 15), (Timer t) => _changeColorByTime());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        iconTheme: const IconThemeData(color: Colors.white),
        title: Text('${defaultColor['text']}, $username',
            style: TextStyle(
                color: Color(defaultColor['textColor']),
                fontSize: 20,
                fontWeight: FontWeight.bold)),
        backgroundColor: Color(defaultColor['bgColor']),
        leading: IconButton(
          onPressed: () => {
            userStore.removeUserName().then((value) =>
                {NavigationService.navigateByPageId(Path.LOGIN, context)})
          },
          icon: const Icon(Icons.lock_outline, color: Colors.white),
        ),
      ),
      body: PageView(
        controller: pageController,
        onPageChanged: _onPageChanged,
        physics: const NeverScrollableScrollPhysics(),
        children: _screens,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(Icons.favorite), label: 'My favorite'),
          BottomNavigationBarItem(icon: Icon(Icons.settings), label: 'Settings'),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: AppTheme.successColor,
        unselectedItemColor: AppTheme.insightsColor,
        onTap: onTapped,
      ),
    );
  }
}
