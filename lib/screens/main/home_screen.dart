import 'package:acn_training_june_2023/utils/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:acn_training_june_2023/model/university.dart';
import 'package:acn_training_june_2023/utils/configuration.dart';
import 'package:acn_training_june_2023/screens/university/university_screen.dart';
import 'package:acn_training_june_2023/storage/user_storage.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Config config = Config();
  String username = "";
  List<University> savedUniversities = [];
  List<dynamic>? universities;
  UserStorage userStorage = UserStorage();

  @override
  void initState() {
    super.initState();
    fetchUniversities().then((data) {
      setState(() {
        universities = data;
      });
    });

    // Retrieve the saved universities from the UserStorage
    userStorage.getUserName().then((un) {
      setState(() {
        username = un ?? "error";
      });

      userStorage.getUniversities(un.toString()).then((universities) {
      setState(() {
        savedUniversities = universities;
      });
    });
    });
    
    
  }

  @override
Widget build(BuildContext context) {
  return Scaffold(
    body: universities != null
        ? Column(
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 0, top: 20),
                    child: Container(
                      constraints: const BoxConstraints.tightFor(
                          width: 350, height: 270),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image.network(
                          config.getApiImg(),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: universities!.length,
                  itemBuilder: (context, index) {
                    final University university =
                        universities![index] as University;

                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => UniversityScreen(
                              university: university,
                            ),
                          ),
                        );
                      },
                      child: ListTile(
                        title: Text(university.name),
                        subtitle: Text(university.country),
                        trailing: IconButton(
                          icon: Icon(
                            savedUniversities
                                    .any((u) => u.name == university.name)
                                ? Icons.star
                                : Icons.star_border,
                            color: savedUniversities
                                    .any((u) => u.name == university.name)
                                ? AppTheme.starredColor
                                : AppTheme.starColor,
                          ),
                          onPressed: () {
                            setState(() {
                              if (savedUniversities
                                  .any((u) => u.name == university.name)) {
                                savedUniversities.removeWhere(
                                    (u) => u.name == university.name);
                              } else {
                                savedUniversities.add(university);
                              }
                              userStorage.saveUniversities(username,savedUniversities);
                            });
                          },
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          )
        : const CircularProgressIndicator(),
  );
}

}
