import 'package:acn_training_june_2023/storage/user_storage.dart';
import 'package:acn_training_june_2023/utils/configuration.dart';
import 'package:acn_training_june_2023/widgets/common_popup.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_jailbreak_detection/flutter_jailbreak_detection.dart';
import 'package:acn_training_june_2023/utils/navigation/navigation_service.dart';
import 'package:acn_training_june_2023/utils/navigation/app_path.dart' as Path;




class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Config config = Config();
  UserStorage userStorage = UserStorage();

  Future<void> getUsername() async {
  }

  void countDownSplash()async {
    await Future.delayed(const Duration(seconds: 3),(){});
    String? storedUsername = await userStorage.getUserName();
    if (storedUsername != null) {
      NavigationService.navigateByPageId(Path.HOME, context);
    }
    else{
      NavigationService.navigateByPageId(Path.LOGIN, context);
    }
  }

  bool? _jailbreak;
  Future<void> checkJailbreak() async {
    bool jailbreak;
    try {
      jailbreak = await FlutterJailbreakDetection.jailbroken;
    } on PlatformException {
      jailbreak = true;
    }
    if (!mounted) return;
    setState(() {
      _jailbreak = jailbreak;
    });
    if (_jailbreak == true) {
      return const AlertPopup().createAlertDialog(
          context, "Alert", "Your phone was jailbreak or root.");
    } else {
      return;
    }
  }

  @override
  void initState() {
    super.initState();
    checkJailbreak();
    countDownSplash();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 255, 255, 255),
      body: SafeArea(
        maintainBottomViewPadding: false,
        child: Stack(
          children:[
            Container(
              alignment: Alignment.center,
              child:Image.asset('asset/images/Peace.png',width: 194,height: 80,
              )),
              
              Align(alignment: Alignment.bottomCenter,
              child:SizedBox(
                width: 805,
                height: 157,
                child: Image.asset('asset/images/Vectors.png'
                ,)
                ,)
                ,)
                ],
            ),
          ),
        );
  }
}
