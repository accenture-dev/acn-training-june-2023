import 'package:acn_training_june_2023/storage/user_storage.dart';
import 'package:acn_training_june_2023/utils/navigation/app_path.dart' as Path;
import 'package:acn_training_june_2023/utils/navigation/navigation_service.dart';

import 'package:flutter/material.dart';
import 'package:acn_training_june_2023/utils/app_theme.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final usernameController = TextEditingController();
  String mobileNumber = "";
  UserStorage userStorage = UserStorage();

  @override
  void initState() {
    super.initState();
  }



  login() async {
    await userStorage.setUserName(usernameController.text);
    navToMain();
  }

  navToMain() async {
    NavigationService.navigateByPageId(Path.HOME, context);
  }

  Widget loginInput() {
    return SafeArea(
      child: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              const SizedBox(
                height: 150,
              ),
              Image.asset(
                'asset/images/Peace.png',
                height: 150,
                width: 194,
              ),
              const Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 60.0),
                  child: Text(
                    "Phone Number",
                    style: TextStyle(
                        color: AppTheme.labelTextColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0),
                  ),
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 50, vertical: 0),
                child: TextFormField(
                  controller: usernameController,
                  maxLength: 24,
                  maxLines: 1,
                  cursorColor: AppTheme.cursorColor,
                  keyboardType: TextInputType.phone,
                  decoration: const InputDecoration(
                    counterText: '',
                    fillColor: AppTheme.inputFillColor,
                    filled: true,
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 17, horizontal: 16),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(color: AppTheme.borderInputColor,),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(
                            color: AppTheme.borderInputColor,
                            width: 1.0)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: AppTheme.borderInputColor,)),
                    hintText: "08XXXXXXX",
                    hintStyle: TextStyle(
                        color: AppTheme.hintTextColor,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal),
                  ),
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: AppTheme.textInputColor),
                  enableSuggestions: true,
                  onChanged: (event) {
                    setState(() {
                      mobileNumber = event;
                    });
                  },
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.only(top: 229),
                  child: SizedBox(
                    width: 805,
                    height: 157,
                    child: Image.asset(
                      'asset/images/Vectors.png',
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    void showMobileNumberInvalidDialog() {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Invalid Mobile Number'),
            content: const Text('Please enter a valid 10-digit mobile number.'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context); // Close the dialog.
                },
                child: const Text('OK'),
              ),
            ],
          );
        },
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        // Wrap the body with SingleChildScrollView
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            loginInput(),
            Container(
              transform: Matrix4.translationValues(0, -380, 0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  fixedSize: const Size(172, 52),
                  backgroundColor: AppTheme.buttonColor,
                  foregroundColor: Colors.white,
                ),
                onPressed: () {
                  if (mobileNumber.isEmpty) {
                    return showMobileNumberInvalidDialog();
                  } else if (mobileNumber.length != 10 || mobileNumber[0] != "0") {
                    showMobileNumberInvalidDialog();
                  } else {
                    login();
                  }
                },
                child: const Text('Login'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}