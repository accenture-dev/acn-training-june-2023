import 'package:acn_training_june_2023/model/university.dart';
import 'package:acn_training_june_2023/widgets/university_list_widget.dart';
import 'package:flutter/material.dart';

class UniversityScreen extends StatelessWidget {
  final University university;
  const UniversityScreen({Key? key,required this.university}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          iconTheme: const IconThemeData(color: Colors.white),
          title: Text(university.name,
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold)),
          backgroundColor: const Color(0xff7413EB),
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: const Icon(Icons.arrow_back, color: Colors.white),
          ),
        ),
        body: UniversityListWidget(university: university));
  }
}
