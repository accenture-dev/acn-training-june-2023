import 'dart:developer';
import 'package:acn_training_june_2023/router.dart';
import 'package:acn_training_june_2023/utils/configuration.dart';
import 'package:acn_training_june_2023/utils/navigation/app_path.dart' as Path;
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future main() async {
  await dotenv.load(fileName: "config.env");
  WidgetsFlutterBinding.ensureInitialized();
  log("Let's we starting application...");
  runApp(const StarterApp());
}

class StarterApp extends StatefulWidget {
  const StarterApp({Key? key}) : super(key: key);

  @override
  _StarterAppState createState() => _StarterAppState();
}

class _StarterAppState extends State<StarterApp> {

  @override
  Widget build(BuildContext context) {
    Config config = Config();
    log("App name ${config.getAppName()}");
    return MaterialApp(
      title: 'Accenture Starter',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.purple,
          backgroundColor: Colors.purple
      ),
      initialRoute: Path.SPLASH,
      onGenerateRoute: RouteGenerator.generateRoute
    );

  }
}