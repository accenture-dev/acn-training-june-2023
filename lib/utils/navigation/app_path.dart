const String SPLASH = "/";
const String LOGIN = "/login";
const String HOME = "/home";
const String UNIVERSITY = "/university";
const String PROFILE = "/profile";
const String SETTINGS = "/settings";