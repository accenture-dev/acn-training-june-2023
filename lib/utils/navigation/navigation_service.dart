import 'package:flutter/material.dart';

class NavigationService {
  static void navigateByPageIdReplacement(String pageId, BuildContext context) {
    Navigator.pushReplacementNamed(context, pageId);
  }

  static void navigateByPageId(String pageId, BuildContext context) {
    Navigator.pushNamed(context, pageId);
  }

  static void navigateAndLeaveMeByPageId(String pageId, BuildContext context) {
    Navigator.pushNamedAndRemoveUntil(context, pageId, (route) => false);
  }
  static void navigateByPageIdReplacementWithArg(String pageId, BuildContext context, Object arg) {
    Navigator.pushReplacementNamed(context, pageId, arguments: arg);
  }
  static void navigateAndLeaveMeByPageIdWithArg(String pageId, BuildContext context, Object arg) {
    Navigator.pushNamedAndRemoveUntil(context, pageId, (route) => false, arguments: arg);
  }
}
