import 'package:flutter/material.dart';

class AppTheme {
  // App theme Color
  static const Color primary = Color(0xFFFFD100);
  static const Color primaryDark = Color(0xFF000F9F);
  static const Color secondary = Color(0xFFF5333F);
  static const Color colorAccent = Color(0xFF0047FF);

  // Messaging color
  static const Color insightsColor = Color.fromRGBO(204, 198, 209, 1);
  static const Color successColor = Color.fromRGBO(124, 58, 237, 1);
  static const Color errorColor = Color(0xFFF05F61);

  static const Color morningColor = Color(0xff58ADF5);
  static const Color dayColor = Color(0xFFF3BB40);
  static const Color eveningColor = Color(0xffF5A558);
  static const Color nightColor = Color(0xff23303D);

  // Login Color
  static const Color borderInputColor = Color.fromRGBO(216, 216, 216, 1);
  static const Color inputFillColor = Color.fromRGBO(245, 245, 245, 1);
  static const Color labelTextColor = Color.fromRGBO(87, 87, 87, 1);
  static const Color cursorColor = Color.fromRGBO(87, 87, 87, 1);
  static const Color hintTextColor = Color.fromRGBO(146, 146, 146, 1);
  static const Color textInputColor = Color.fromARGB(255, 80, 80, 80);
  static const Color buttonColor = Color.fromRGBO(167, 129, 244, 1);

  // Home Color
  static const Color starredColor = Color.fromRGBO(244, 114, 182, 1);
  static const Color starColor = Color.fromRGBO(215, 215, 215, 1);
}
