import 'dart:developer';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class Config {

  String getUniList() {
    return dotenv.env['API_PROVINCE'] ?? "";
  }

  String getApiImg() {
    return dotenv.env['API_IMAGE'] ?? "";
  }

  String getAppName() {
    return dotenv.env['APP_NAME'] ?? "unknown";
  }

  int getSplashTime() {
    String timeStr = dotenv.env['SPLASH_TIME'] ?? "2";
    try {
      int time = int.parse(timeStr);
      return time;
    } catch(ex) {
      log("Error while parse splash time config ${ex.toString()}");
      return 2;
    }
  }
}