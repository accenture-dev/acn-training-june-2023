import 'dart:convert';
import 'package:acn_training_june_2023/model/university.dart';

class Universities {
  List<University> list = [];

  Universities({
    required this.list
  });

  parseToJsonList(json) {
    var aaa = json["list"].map((e) => University.fromJson(e));
    return Universities(list: aaa);
  }

  factory Universities.fromJson(dynamic json) => Universities(list: json["list"].map((e) => University.fromJson(e)));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    List<Map<String, dynamic>> listJson = list.map((e) => e.toJson()).toList();
    data['list'] = listJson;
    return data;
  }

  String toJsonString() {
    var uni = Universities(list: list);
    var data = json.encoder.convert(uni);
    return data;
  }
}
