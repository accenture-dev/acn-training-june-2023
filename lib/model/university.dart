import 'package:http/http.dart' as http;
import 'package:acn_training_june_2023/utils/configuration.dart';
import 'dart:convert';


Future<List<University>> fetchUniversities() async {
  Config config = Config();
  final response = await http.get(Uri.parse(config.getUniList()));

  if (response.statusCode == 200) {
    final List<dynamic> jsonList = jsonDecode(response.body);
    final List<University> universities = jsonList.map((json) => University.fromJson(json)).toList();
    return universities;
  } else {
    throw Exception('Failed to fetch universities');
  }
}

class University {
  String name = "";
  String alphaCode = "";
  List domains = [];
  String stateProvince = "";
  List webPages = [];
  String country = "";

  University({
    required this.name,
    required this.alphaCode,
    required this.domains,
    required this.stateProvince,
    required this.webPages,
    required this.country
  });

  factory University.mock() => University(
      name: "...",
      alphaCode: "...",
      domains: [],
      stateProvince: "...",
      webPages: [],
      country: "...");

  factory University.fromJson(Map<String, dynamic> json) =>
      University(
          name: json["name"] ?? "",
          alphaCode: json["alpha_two_code"] ?? "",
          domains: json["domains"] ?? [],
          stateProvince: json["state-province"] ?? "",
          webPages: json["web_pages"] ?? [],
          country: json["country"] ?? "",
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['alpha_two_code'] = alphaCode;
    data['domains'] = domains;
    data['state-province'] = stateProvince;
    data['web_pages'] = webPages;
    data['country'] = country;
    return data;
  }
}
