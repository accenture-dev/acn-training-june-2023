class UserModel {
  String phone = "";
  List<String> fav = [];

  UserModel({
    required this.phone,
    required this.fav,
  });

  factory UserModel.mock() => UserModel(
        phone: "...",
        fav: [],
      );

  
}
