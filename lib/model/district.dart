class District {
  String provinceCode;
  String provinceEn;
  String provinceTh;
  String districtCode;
  String districtEn;
  String districtTh;
  District(
      {this.provinceCode = '...',
      this.provinceEn = '...',
      this.provinceTh = '...',
      this.districtCode = '...',
      this.districtEn = '...',
      this.districtTh = '...'});

  factory District.fromJson(Map<String, dynamic> json) {
    return District(
        provinceCode: json['provinceCode'],
        provinceEn: json['provinceEn'],
        provinceTh: json['provinceTh'],
        districtCode: json['districtCode'],
        districtEn: json['districtEn'],
        districtTh: json['districtTh']);
  }

  remove() {
    provinceCode = '...';
    provinceEn = '...';
    provinceTh = '...';
    districtCode = '...';
    districtEn = '...';
    districtTh = '...';
  }
}
