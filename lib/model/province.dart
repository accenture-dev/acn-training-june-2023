class Province {
  String provinceCode;
  String provinceEn;
  String provinceTh;
  Province({
    this.provinceCode = '...',
    this.provinceEn = '...',
    this.provinceTh = '...',
  });

  factory Province.fromJson(Map<String, dynamic> json) {
    return Province(
      provinceCode: json['provinceCode'],
      provinceEn: json['provinceEn'],
      provinceTh: json['provinceTh'],
    );
  }

  remove() {
    provinceCode = '...';
    provinceEn = '...';
    provinceTh = '...';
  }
}
