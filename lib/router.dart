import 'dart:developer';
import 'package:acn_training_june_2023/screens/auth/login_screen.dart';
import 'package:acn_training_june_2023/screens/auth/splash_screen.dart';
import 'package:acn_training_june_2023/screens/main/main_screen.dart';
import 'package:acn_training_june_2023/model/university.dart';
import 'package:acn_training_june_2023/screens/university/university_screen.dart';
import 'package:acn_training_june_2023/screens/main/settings_screen.dart';
import 'package:flutter/material.dart';
import 'package:acn_training_june_2023/utils/navigation/app_path.dart' as Path;

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Path.SPLASH:
        return MaterialPageRoute(builder: (context) => const SplashScreen());
      case Path.LOGIN:
        return MaterialPageRoute(builder: (context) => const LoginScreen());
      case Path.HOME:
        return MaterialPageRoute(builder: (context) => const MainScreen());
      case Path.UNIVERSITY:
        return MaterialPageRoute(builder: (context) => UniversityScreen(university: University.mock()));
      case Path.SETTINGS:
        return MaterialPageRoute(builder: (context) => const SettingsScreen());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (BuildContext context) {
      log('Error Route');
      return Scaffold(
        appBar: AppBar(title: const Text('Not Found')),
        body: const Center(child: Text('Not Found')),
      );
    });
  }
}
